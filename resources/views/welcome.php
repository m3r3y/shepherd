<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <title>Игра пастух</title>


  <!-- CSS -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script type="text/javascript">
    var _token = "<?php echo csrf_token(); ?>";
  </script>
</head>
<body>

  <!-- navigation bar -->


   <!-- main body of our application -->
  <div  class="container" id="game">
   
    <div 
        is='error'
        v-bind:msg='errorMsg'
    ></div>

    <h1 v-html="title"></h1>
    <h3 v-if="isStopped" v-html="mission"></h3>

    <h3 v-if="isAcitve" ><span v-html="dayString"></span>. <span v-html="countTotal()"></span></h3>
    <h4 v-if="isAcitve" v-html="info"></h4>
    

    <input v-if="isStopped" v-on:click="gameStart()"  type="submit" class="avacado" value="Начать игру" />
    <input v-if="isAcitve"  v-on:click="gameStop()"   type="submit" class="avacado" value="Завершить игру" />

    <div v-if="isAcitve" id="gameYards">
      
        <div
          is="yard"
          v-for="(id,yard) in sheepsInYard"
          v-bind:sheep="id"
          v-bind:yard="yard"
          v-bind:quanity="getSheepInYard(yard)"
        >
          
        </div>

    </div>
  

  </div>


  

  <!-- JS -->
  <script src="js/vue.min.js"></script>
  <script src="js/vue-resource.min.js"></script>
  <script src="js/sheep.js"></script>
</body>
</html>