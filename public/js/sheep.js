
var url = 'http://shepherd.lan/';

var api = {

  'game' : {
      
      'GET'    : url + 'api/game/show',   // Get info               //
      'PUT'    : url + 'api/game/',       // {id} Update day        //
      'POST'   : url + 'api/game',        // Create new game        //
      'DELETE' : url + 'api/game/',       // Destroy game           //
  },

  'sheep' : {

      'GET'    : url + 'api/sheep/show',  // {id} Get info           //
      'PUT'    : url + 'api/sheep/',      // {id} Update day         //
      'POST'   : url + 'api/sheep',       // {id} Create new game    //
      'DELETE' : url + 'api/sheep/',      // {id} Destroy game       //

  },

  'journal' : {

      'GET'    : url + 'api/journal/show',  // {id} Get info           //
  }

};

var settings = {

  delay:    10000,   // Длительность дня (в милисекундах) //
  period:   10,      // Продолжительность, периода когда забирают овечку (в днях)   //

}

// Здесь происходит магия. на рефакторинг ! //

Vue.component('yard', {
  template: "<div class='yard'> <div class='yard-title'> Загон: {{yard}}. Всего: {{quanity}}</div><div class='sheep' v-for='(sh,index) in sheep'> <span class=\"label label-default sheepId \">{{sh}}</span></div></div>",
  props: ['sheep','yard', 'quanity'],
});

Vue.component('error', {
  template: "<h1>{{msg}}</h1>",
  props: ['msg'],
});


// Управление игрой //

var game = new Vue({

  data : {
      api :         api,
      external :    {},
      title :       "Игра Пастух",
      mission :     "* Ваша миссия следить за стадом и считать овец.",
      dayDelay :    settings.delay,
      periodLength: settings.period,
      connection:   true,
      timer :       0,
      errorMsg:     "",
      info :        "Событие дня: игра началась",
      countAdd :    0,
      countRemove : 0,
      countMove :   0,
      history :     false,
  },

  // Вычисляемые свойства //

  computed: {
      id : function(){
          return this.external.id;
      },

      idString : function()
      {
          return "Идентификатор игры - " +  this.external.id;
      },

      state : function(){
          return this.external.state;
      },


      stateString : function(){
          return this.external.state;
      },


      isAcitve : function(){
          if ( this.external.state === 'active' ) {
             return true;
          } else {
             return false;
          }
      },

      isStopped : function(){
          if ( this.external.state === 'stopped' ) {
             return true;
          } else {
             return false;
          }
      },

      isEnded : function(){

          if ( this.history === 'stopped' ) {
             return true;
          } else {
             return false;
          }

      },

      day : function(){
          return this.external.day;
      },

      dayString : function()
      {
          return "День : " +  this.external.day;
      },

      yards : function(){
          return this.external.yards;
      },

      sheepsInYard : function(){
          return this.external.sheeps;
      },


  },

  // Привязка к элементу //


  el : "#game",

  // Методы //

  methods : {



      // -- Game Control -- //

      gameGetData : function()
      {
          // Заглушка, для данных пока не придут реальные данные //

         this.external = {
         }

          // А вот реальные данные //

          this.serverGameGet();          
      },

      // Начать новую игру //

      gameStart : function()
      {          
          this.timer = this.init(this.dayDelay);
          this.serverGameStart();

          console.log('Game Started');
      },

      // Закончить игру //

      gameStop : function()
      {
          this.external.state = "stopped";
          this.clear(this.timer);
          this.serverGameStop();

          console.log('Game stopped');
      },

      // Обновление состояния при повторной загрузке //

      gameRefresh : function()
      {   

          if ( this.timer === 0 || this.timer === undefined ) {
               this.timer = this.init(this.dayDelay);
          }  
             
      },




      //  Запуск таймера игры //

      init : function(delay)
      {
          var start = setInterval(function() {
                game.update();
          }, delay);

          return start;
      },

      //  Сброс таймера //

      clear : function(id)
      {
          return clearInterval(id);
      },

      

      //  Методы, которые требуют обновления  //

      update : function()
      {
          this.oneDayLeft();

          if ( (this.external.day % this.periodLength) === 0 )  {
            this.onePeriodLeft();
          }
      },

      //  Все, что происходиит за один день //

      oneDayLeft : function()
      {
          this.info = "Событие дня: ";

          //  Инкремент для дня //

          this.external.day++;

          //  Добавить один день на сервере //
          //  Увеличиваем время выполнения, чтобы не нагружать сервер //
      
          setTimeout(function(){
              game.serverGameDayUpdate();
          }, 300); 

          //  Размножение раз в день //

          this.reproduction();

          console.log("Day:" + this.external.day);
      },

      //  Все, что происходиит в один период //

      onePeriodLeft : function()
      {
          //  День забоя, один раз в период //

          this.slaughtering();

          console.log(this.external.day + " - period");
      },


      //  -- Game mechanics -- //


      sheepGetYard : function(id)
      {
          var sheeps = this.external.sheeps;

          for(var i = 1; i <= this.yards; i++ )
          {
            for (var j = 0; j < sheeps[i].length; j++ ) {

                if (sheeps[i][j] === id) return i;

            }
          }

          return false;
      },

      sheepRemove : function(yard, id)
      {
          var sheeps = this.external.sheeps[yard];
          var sheep  = sheeps.indexOf(id);

          if (sheep > -1) {
              sheeps.splice(sheep, 1);
          }

          setTimeout(function(){
              game.serverSheepRemove(id);
          }, 300);

          this.countRemove++;
      },



      sheepAdd : function(yard) 
      {
          // Костыль, потому что данные загружаются с сервера в последнюю очередь, и мы не знаем id на сервере. //
          // Поэтому, сами генерируем id //

          id = 0;

          var sheeps = this.external.sheeps;

          for(var i = 1; i <= this.yards; i++ )
          {
            for (var j = 0; j < sheeps[i].length; j++ ) {

                if (sheeps[i][j] > id) {
                    id = sheeps[i][j];
                }

            }
          }

          id = id + 2;

          // Увеличиваем время выполнения, чтобы не нагружать сервер //

          setTimeout(function(){
              game.serverSheepAdd(yard);;
          }, 300);
          
          this.external.sheeps[yard].push(id);

          this.countAdd++;
      },

      sheepUpdate : function(id, yard)
      {
          var old_yard = this.sheepGetYard(id);
          var new_yard = yard;

          var sheeps = this.external.sheeps[old_yard];
          var sheep  = sheeps.indexOf(id);

          if (sheep > -1) {
                sheeps.splice(sheep, 1);
          }

          // Увеличиваем время выполнения, чтобы не нагружать сервер //

          setTimeout(function(){
              game.serverSheepUpdate(new_yard, id);
          }, 300); 

          this.external.sheeps[new_yard].push(id);

          this.countMove++;
      },

      reproduction : function()
      {
        var yard    = this.random(1,this.yards);

        var quanity = this.external.sheeps[yard].length;

        if ( quanity <= 1) {

              // Рефакторинг.  Рекурсия, которая может уйти в бесконечность, если в загонах по одной овечке //

              this.reproduction();

              return false;
        }

        this.sheepAdd(yard);

        this.setEvent('в загоне ' + yard + ' появилась овечка ');


        for(var i = 1; i <= this.yards; i++ )
        {
              var quanity = this.external.sheeps[i].length;

              if (  quanity === 1 ) {
                   this.singleSheep([i]);
              }
        }

      },

      slaughtering : function()
      {

          var yard    = this.random(1,this.yards);

          var quanity = this.external.sheeps[yard].length;

          if ( quanity <= 1) {
              this.slaughtering();
              return false;
          }

          var index = this.random(0,quanity);

          id = this.external.sheeps[yard][index];

          this.sheepRemove(yard,id);

          this.setEvent('Овечке "' + id + '" из загона ' + yard + ' сегодня не повезло');

      },

      singleSheep : function(yard)
      {
          var maxYard   = 0;
          var max       = 0;

          for (var i = 1; i <= this.yards; i++)
          {
              quanity = this.external.sheeps[i].length;

              if ( quanity > max ) {
                  max     = quanity;
                  maxYard = i;
              }
          }

          id = this.external.sheeps[maxYard][0];

          this.sheepUpdate(id,yard);

          this.setEvent('Овечка "' + id + '" из загона ' + maxYard + ' пересажена в загон ' + yard);

      },

      countTotal : function()
      {
          var total = 0;

          for (var i = 1; i <= this.yards; i++)
          {
              quanity = this.external.sheeps[i].length;
              total  += quanity;
          }

          return "Овец: " + total;
      },

      getSheepInYard : function(yard)
      {
          return this.external.sheeps[yard].length;
      },

      setEvent : function(event)
      {
          var event = this.info + event + ", ";
          this.info = event;
      },

      random : function (min, max) 
      {
          var rand = min - 0.5 + Math.random() * (max - min + 1)
          rand = Math.round(rand);
          return rand;
      },

      showHistory : function()
      {
      },

      autoReload : function (){
            var goal = self.location;
            location.href = goal;
      },

      // -- Server Request -- //


      serverError : function()
      { 

          this.error = true;
          this.errorMsg = 'Что-то сломалось :*( ';

          return  console.log('Error: Game data not found!');
      },

      serverConnectionError : function()
      { 
          this.connection = false;
          this.errorMsg   = "Потерянно соеденение с сервером!";

          return console.log('ConnectionError');
      },


      serverGameStart : function()
      {
          
          this.$http.post(this.api.game.POST, { _token : _token } ).then((response) => {

              // Рефакторинг //
              // Обработка внутренней ошибки сервера  //

              location.reload();


          }, (response) => {
                this.serverConnectionError();
          });

          return console.log('Server Game started');
      },



      serverGameStop : function()
      {
          
          // vue-resource не хочет отправлять метод DELETE //
          // имитируем DELETE через POST //

          this.$http.post(this.api.game.DELETE + this.external.id, { _token : _token, _method : 'DELETE' } ).then((response) => {

              

          }, (response) => {
                this.serverConnectionError();
          });

          return console.log('Server Game Stopped');
      },

      serverGameGet : function()
      {

          this.$http.get(this.api.game.GET).then((response) => {

              if ( response.body.state === undefined ) {
                  console.log(response.body);
              }

                console.log('Server Data Updated');

                this.external = response.body;
            

          }, (response) => {
                this.serverConnectionError();
          });

          return console.log('Server Data Updated');
      },


      serverGameDayUpdate : function()
      {

          this.$http.put(this.api.game.PUT + this.external.id, { _token : _token } ).then((response) => {

              // Рефакторинг //
              // Обработка внутренней ошибки сервера  //

          }, (response) => {
                this.serverConnectionError();
          });


      },

      serverSheepAdd : function(yard_id)
      {
          this.$http.post(this.api.sheep.POST, { _token : _token, yard : yard_id } ).then((response) => {

              // Рефакторинг //
              // Обработка внутренней ошибки сервера  //

          }, (response) => {
                this.serverConnectionError();
          });

      },

      serverSheepUpdate : function(yard_id, sheep_id)
      {   

          this.$http.put(this.api.sheep.PUT + sheep_id, { _token : _token, yard : yard_id } ).then((response) => {

              

          }, (response) => {
                this.serverConnectionError();
          });

          return console.log('Sheep Updated');
      },

      serverSheepRemove : function(sheep_id)
      {
          // vue-resource не хочет отправлять метод DELETE //
          // имитируем DELETE через POST //

          this.$http.post(this.api.sheep.DELETE + sheep_id, { _token : _token, _method : 'DELETE' } ).then((response) => {
              
               // Рефакторинг //
              // Обработка внутренней ошибки сервера  //

          }, (response) => {
                this.serverConnectionError();
          });

      },

      serverSheepGet : function()
      {
          return console.log('Sheep getting');
      },

      serverJournalGet : function()
      {
          return console.log('Sheep getting');
      },

      
  },

  // Запуск методов при загрузке  //

  mounted : function(){

      // Проверяем запущена ли игра  //
      this.gameRefresh();

      // Данные из сервера  //
      this.gameGetData();
  }

});
