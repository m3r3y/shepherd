<?php

/**
 * 
 * Роуты в основном для api
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */


Route::get('/', function () {
    return view('welcome');
});

/** api  управления игрой **/
Route::resource('api/game', 	'gameRESTController');

/** api  управления овечками **/
Route::resource('api/sheep', 	'sheepRESTController');

/** api  управления журналом действий **/
Route::resource('api/journal', 	'journalRESTController');

