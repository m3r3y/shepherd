<?php

namespace App\Models\Entities;

/**
 * Сущность модели Овечки
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Database\Eloquent\Model;

class Sheep extends Model
{
    /**
	 * @property string 
     * @var Название таблицы
     */

    protected $table = "sheep";

    /**
	 * @property array 
     * @var Поля обязательные для заполнения
     */

    protected $fillable = ['game_id', 'yard'];

    /**
	 * @property array 
     * @var hidden
     */

    protected $hidden = [];
}
