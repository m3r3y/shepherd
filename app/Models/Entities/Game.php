<?php

namespace App\Models\Entities;

/**
 * Сущность модели Игры
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
	/**
	 * @property string 
     * @var Название таблицы
     */

    protected $table = "game";

    /**
	 * @property array 
     * @var Поля обязательные для заполнения
     */

    protected $fillable = ['state'];

    /**
	 * @property array 
     * @var hidden
     */

    protected $hidden = [];
}
