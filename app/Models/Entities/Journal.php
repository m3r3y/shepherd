<?php

namespace App\Models\Entities;

/**
 * Сущность модели Журнала
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
     /**
	 * @property string 
     * @var Название таблицы
     */

    protected $table = "journal";

    /**
	 * @property array 
     * @var Поля обязательные для заполнения
     */

    protected $fillable = ['game_id', 'action'];

    /**
	 * @property array 
     * @var hidden
     */

    protected $hidden = [];
}
