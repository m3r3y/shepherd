<?php

namespace App\Models\Repositories;

/**
 * Интерфейс репозитория для управления данными по овечкам
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

interface SheepRepoInterface {

     /**
     * Новая овечка
     *
     * @param int $game_id
     * @param int $yard
     * @return int | bool
     */

     public function addNew($game_id, $yard);

	/**
     * Убить овечку
     *
     * @param int $sheep_id
     * @return int | bool
     */

	public function kill($sheep_id);

     /**
     * Добавить множество овечек
     *
     * @param array $sheeps
     * @return int | bool
     */

     public function insert($sheeps);

	/**
     * Получить сессию игры овечки 
     *
     * @param int $sheep_id
     * @return int | bool
     */

	public function getGameId($sheep_id);

	/**
     * Получить номер загона в которой находиться овечка
     *
     * @param int $sheep_id
     * @return int | bool
     */

	public function getYard($sheep_id);

	/**
     * Установить номер загона для 
     *
     * @param int $sheep_id
     * @param int $yard
     * @return bool
     */

	public function setYard($sheep_id, $yard);

	/**
     * Получить состояние овечки
     *
     * @param int $sheep_id
     * @return string | bool
     */

	public function getState( $sheep_id );

	/**
     * Установить состояние овечки 
     *
     * @param int $sheep_id
     * @param string $state
     * @return bool
     */

	public function setState( $sheep_id, $state );

     /**
     * Получить всех живых овечек в игре
     *
     * @param int $sheep_id
     * @return array
     */

     public function getAlive($game_id);
     

      /**
     * Получить всех овечек, которые пошли на мясо в игре
     *
     * @param int $game_id
     * @return array
     */

     public function getMeat($game_id);
    

}