<?php

namespace App\Models\Repositories;

/**
 * Репозиторий для управления данными по овечкам
 * Используется как обертка для Eloquent Model
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Database\Eloquent\Model;
use App\Models\Repositories\SheepRepoInterface;
use App\Models\Entities\Sheep;

class SheepRepo implements SheepRepoInterface {

	/**
	 * @property object 
     * @var Eloquent Model object
     */

	private $eloquentModel;


	public function __construct( Model $model )
	{
		$this->eloquentModel = $model;
	}

	/**
     * Новая овечка
     *
     * @param int $game_id
     * @param int $yard
     * @return bool
    */

     public function addNew($game_id, $yard)
     {
     	$this->eloquentModel->game_id 	= $game_id;
     	$this->eloquentModel->yard 		= $yard;
        $this->eloquentModel->save();

        return $this->eloquentModel->id;
     }

     /**
     * ? Добавить множество овечек 
     *
     * @param array $sheeps
     * @return bool
     */

     public function insert($sheeps)
     {
     	foreach ($sheeps as $key => $sheep) {

     		if ( ! $this->eloquentModel->create(['game_id' => $sheep['game_id'], 'yard' => $sheep['yard']])) {
     			return flase;
     		}

     	}

     	return true;
     }

	/**
     * Убить овечку
     *
     * @param int $sheep_id
     * @return bool
     */


	public function kill($sheep_id)
	{    
        return $this->setState($sheep_id, 'meat');
    }

	/**
     * Получить сессию игры овечки 
     *
     * @param int $sheep_id
     * @return int | bool
     */

	public function getGameId($sheep_id)
	{
		return 1;
	}


	/**
     * Получить номер загона в которой находиться овечка
     *
     * @param int $sheep_id
     * @return int | bool
     */

	public function getYard($sheep_id)
	{
		$var = $this->eloquentModel->find($sheep_id);
        
        if ( ! isset($var)) {
             return false;
        }
            
        return $var->yard;
	}

	/**
     * Установить номер загона для 
     *
     * @param int $sheep_id
     * @param int $yard
     * @return bool
     */

	public function setYard($sheep_id, $yard)
	{
		$var = $this->eloquentModel->find($sheep_id);
        
        if ( ! isset($var)) {
        	 return false;
        }
        
        $var->yard   = $yard;
        $var->save();
            
        return true;
	}

	/**
     * Получить состояние овечки
     *
     * @param int $sheep_id
     * @return string | bool
     */

	public function getState( $sheep_id )
	{
		$var = $this->eloquentModel->find($sheep_id);
        
        if ( ! isset($var)) {
        	 return false;
        }
            
        return $var->state;
	}

	/**
     * Установить состояние овечки 
     *
     * @param int $sheep_id
     * @param string $state
     * @return bool
     */

	public function setState( $sheep_id, $state )
	{
		$var = $this->eloquentModel->find($sheep_id);
        
        if ( ! isset($var)) {
        	 return false;
        }
        
        $var->state   = $state;
        $var->save();
            
        return true;
	}

	/** Получить всех живых овечек в игре
     *
     * @param int $game_id
     * @return array
     */

     public function getAlive($game_id)
     {
     	$result = [];

     	$sheeps = $this->eloquentModel->where('game_id', $game_id)->get(); 

     	foreach ($sheeps as $sheep)
		{
			if ( $sheep->state !== 'alive' ) {
				continue;
			}
			$result[$sheep->id]['id']   = $sheep->id;
			$result[$sheep->id]['yard'] = $sheep->yard;
		}

		return $result;
     }

      /**
     * Получить всех овечек, которые пошли на мясо в игре
     *
     * @param int $game_id
     * @return array
     */

     public function getMeat($game_id)
     {
     	$result = [];

     	$sheeps = $this->eloquentModel->where('game_id', $game_id)->get(); 

     	foreach ($sheeps as $sheep)
		{
			if ( $sheep->state !== 'meat' ) {
				continue;
			}
			
			$result[$sheep->id]['id']   = $sheep->id;
			$result[$sheep->id]['yard'] = $sheep->yard;
		}

		return $result;
     }




}