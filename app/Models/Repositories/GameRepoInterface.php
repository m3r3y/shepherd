<?php

namespace App\Models\Repositories;

/**
 * Интерфейс репозитория для управления данными по игре
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

interface GameRepoInterface {

     /**
     * Добавить новую игру
     *
     * @return bool
     */

     public function addNew();

	/**
     * Получить id текущей игры
     *
     * @return int | bool
     */

	public function getId();

	/**
     * Получить состяние игры  
     *
     * @param int $game_id
     * @return int | bool
     */


	public function getState($game_id);

	/**
     * Установить состояние игры 
     *
     * @param int $sheep_id
     * @param int $yard
     * @return bool
     */

	public function setState($game_id, $state);

	/**
     * Получить текущий день игры
     *
     * @param int $sheep_id
     * @return int | bool
     */


	public function getDay($game_id);

	/**
     * Установить количество дней 
     *
     * @param int $game_id
     * @param int $days
     * @return bool
     */

	public function setDay($game_id, $days);

     /**
     * Увеличить кол-во дней на один
     *
     * @param int $game_id
     * @return bool
     */

     public function setOneDay($game_id);




}