<?php

namespace App\Models\Repositories;

/**
 * Репозиторий для управления данными по игре
 * Используется как обертка для Eloquent Model
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Database\Eloquent\Model;
use App\Models\Repositories\GameRepoInterface;
use App\Models\Entities\Game;

class GameRepo implements GameRepoInterface {

	/**
	 * @property object 
     * @var Eloquent Model object
     */

	private $eloquentModel;

	/**
     * В конструкторе передается модель сущность GameEntity
     *
     * @param object  Model $model
     * @return void
     */

	public function __construct( Model $model )
	{
		$this->eloquentModel = $model;
	}

	/**
     * Добавить новую игру
     *
     * @return bool
     */

    public function addNew()
    {
          $this->eloquentModel->state = "active";

          if ($this->eloquentModel->save()) {
               return true;
          } else {
               return false;
          }
    }

	/**
     * Получить id последней игры
     *
     * @return int | bool
     */

	public function getId()
	{
		$game = $this->eloquentModel->latest()->first();

          if ( ! isset($game) ) {

               /** Таблица пуста, следовательно id = 1 **/

               return 1;
          }

          return $game->id;
	}

	/**
     * Получить состяние игры  
     *
     * @param int $game_id
     * @return string | bool
     */

	public function getState($game_id)
	{
		$game = $this->eloquentModel->find($game_id);

          if ( ! isset($game->state) ) {
               return false;
          }

          return $game->state;
	}

	/**
     * Установить состояние игры 
     *
     * @param int $sheep_id
     * @param int $yard
     * @return bool
     */

	public function setState($game_id, $state)
	{    
          $var = $this->eloquentModel->find($game_id);
          $var->state   = $state;

          if ($var->save()) {
               return true;
          } else {
               return false;
          }
	}

	/**
     * Получить текущий день игры
     *
     * @param int $sheep_id
     * @return int | bool
     */


	public function getDay($game_id)
	{
		$game = $this->eloquentModel->find($game_id);

          if ( ! isset($game->day) ) {
               return false;
          }

          return $game->day;
	}

	/**
     * Установить количество дней 
     *
     * @param int $game_id
     * @param int $days
     * @return bool
     */

	public function setDay($game_id, $days)
	{
		$var = $this->eloquentModel->find($game_id);
          $var->day   = $days;

          if ($var->save()) {
               return true;
          } else {
               return false;
          }
	}

     /**
     * Увеличить кол-во дней на один
     *
     * @param int $game_id
     * @param int $days
     * @return bool
     */

     public function setOneDay($game_id)
     {
          $var = $this->eloquentModel->find($game_id);
          $var->day++;

          if ($var->save()) {
               return true;
          } else {
               return false;
          }
     }

	

}