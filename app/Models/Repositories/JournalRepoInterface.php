<?php

/**
 * Репозиторий для данных по журнала
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

namespace App\Models\Repositories;

interface JournalRepoInterface {

	/**
     * Добавить новую запись в журнал 
     *
     * @param int 	$gameId - Сессия игры
     * @param string $action - Действие
     * @param int $sheep_id - id овечки
     * @param int $yard 
     * @param int $day 
     * @return bool
     */

	public function setEntry($gameId, $action, $sheep_id, $yard, $day);

     /**
     * Вывести журнал по определнной игре 
     *
     * @param int $gameId - id игры
     * @return array
     */

	public function getAll($gameId);

     /**
     * Вывести журнал за определенный день 
     *
     * @param int $gameId - id игры
     * @param int $day - день
     * @return array
     */

	public function getDay($gameId, $day);

     /**
     * Вывести журнал по определенному загону
     *
     * @param int $gameId - id игры
     * @param int $day - yard
     * @return array
     */

	public function getYard($game_id, $yard);

}