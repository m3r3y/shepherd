<?php

namespace App\Models\Repositories;

/**
 * Репозиторий для управления данными по журналу
 * Используется как обертка для Eloquent Model
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Database\Eloquent\Model;
use App\Models\Repositories\JournalRepoInterface;
use App\Models\Entities\Journal;

class JournalRepo implements JournalRepoInterface {

	/**
	 * @property object 
     * @var Eloquent Model object
     */

	private $eloquentModel;

	/**
     * В конструкторе передается модель сущность JournalEntity
     *
     * @param object  Model $model
     * @return void
     */

	public function __construct( Model $model )
	{
		$this->eloquentModel = $model;
	}

	/**
     * Добавить новую запись в журнал 
     *
     * @param int 	$gameId - Сессия игры
     * @param string $action - Действие
     * @param int $sheep_id - id овечки
     * @param int $yard 
     * @param int $day 
     * @return bool
     */

	public function setEntry($gameId, $action, $day = null, $yard = null, $sheep_id = null)
	{
		$entry = $this->eloquentModel;

		$entry->game_id 	= $gameId;
		$entry->action  	= $action;
		$entry->sheep_id  	= $sheep_id;
		$entry->yard  		= $yard;
		$entry->day  		= $day;



		if ($entry->save()) {

				$entry = false;
               	return true;
        } else {
               return false;
        }
	}

	/**
     * Вывести все данные журнала по определенной игре
     *
     * @param int  $game_id
     * @return array
     */

	public function getAll($game_id)
	{

        $journals = $this->eloquentModel->where('game_id', $game_id)->get();

        $result = [];

        foreach ($journals as $journal)
		{
			$result[$journal->id]['id'] 	= $journal->id;
			$result[$journal->id]['action'] = $journal->action;
			$result[$journal->id]['day'] 	= $journal->day;
			$result[$journal->id]['yard'] 	= $journal->yard;
			$result[$journal->id]['sheep'] 	= $journal->sheep_id;
		}

		return $result;

	}

	/**
     * Это просто заглушка, для будущей функции
     *
     * @param int  $game_id
     * @return array
     */

	public function getDay($gameId, $day)
	{	
		return true;
	}

	/**
     * Это просто заглушка, для будущей функции
     *
     * @param int  $game_id
     * @return array
     */

	public function getYard($game_id, $yard)
	{
		return true;
	}

}