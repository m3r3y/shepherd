<?php

namespace App\Models\Services;

/**
 * Сервис сборщик данных по игре
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Database\Eloquent\Model;
use App\Models\Repositories\GameRepoInterface;
use App\Models\Repositories\GameRepo;
use App\Models\Repositories\SheepRepoInterface;
use App\Models\Repositories\SheepRepo;
use App\Models\Repositories\JournalRepoInterface;
use App\Models\Repositories\JournalRepo;
use App\Models\Entities\Game;
use App\Models\Entities\Sheep;
use App\Models\Entities\Journal;


class GameBuilder implements GameBuilderInterface {


	private $game;
	private $sheep;
	private $journal;

     /**
     * @property Массив для хранения настроек
     * @var array 
     */
     private $settings = [
          'sheep_at_start'    => 12,
          'yards_quanity'     => 6,
     ];

	/**
	 * @property Массив для хранения кодов и описаний ошибок 
     * @var array 
     */

	private $error = [

          '0'      => 'Unkown Error',
          '1'      => 'Game already started, you should finish it.',
          '2'      => 'Game already stopped.',
          '3'      => 'Data Base Error.',
          '4'      => 'Game is stopped. Command failed',
          '5'      => 'You couldn\'t move this sheep ',
		'404'    => 'Not found',

	];

	/**
	 * @property Массив для хранения кодов и описаний успешного выполнения
     * @var array 
     */

	private $success 		= [
		'1'     => 'All tasks done',
	];

	/**
     * Определение основных заивисимостей. 
	 *
     * @param void
     * @return void
     */

	public function __construct()
	{
		$entitity 	= new Game();
		$this->game 	= new GameRepo($entitity);

          $entitity      = new Sheep();
          $this->sheep   = new SheepRepo($entitity);

          $entitity      = new Journal();
          $this->journal = new JournalRepo($entitity);
	}

	/**
     * Запустить новую игру. 
     * Успешно, возвращается массив с ключем success
     * Если игра уже запущена, возвращается массив с кодом ошибки и описанием
     *
     * @return array
     */

	public function start()
	{

          if ( $this->currentGameState() === 'active' ) {
               return $this->error('1');
          }

          if ( $this->newGame() === false ) {
               return $this->error('3'); 
          }

          $sheeps_count       = $this->settings['sheep_at_start'];
          $yards_quanity      =  $this->settings['yards_quanity'];
          $sheeps             = $this->generateSheeps($sheeps_count, $yards_quanity );

          $game_id            = $this->game->getId();
          $sheeps             = $this->setGameId($game_id,$sheeps);

          if ( $this->saveGeneratedSheeps($sheeps) === false ) {
               return $this->error('3'); 
          }

          /** Регистрация в журнале */

          $this->newJournalEntry($game_id, "start");

          return $this->success(1);
		
	}

	/**
     * Остановить игру текущую игру.
     * Успешно, возвращается массив с ключем success
     * Ошибка, возвращается массив с ключем error, кодом ошибки и описанием.
     *
     * @return array
     */

	public function stop()
	{
          if ( $this->currentGameState() === 'stopped' ) {
               return $this->error('2');
          }

          if ( $this->endGame() === false) {
               return $this->error();
          }

          /** Регистрация в журнале */

          $game_id  = $this->game->getId();
          $day      = $this->game->getDay($game_id);


          $this->newJournalEntry($game_id, "stop", $day);

          return $this->success(1);
	}

	/**
     * Добавить одинь день к текущей игре
     * Успешно, возвращается массив с ключем success
     * Ошибка, возвращается массив с ключем error, кодом ошибки и описанием.
     *
     * @return array
     */

	public function update()
	{   
          if ( $this->currentGameState() === 'stopped' ) {
               return $this->error('4');
          }

          $game_id = $this->game->getId(); 

          if ( $this->game->setOneDay($game_id) ) {
               return $this->success(1);
          } else {
               return $this->error('2');
          }	
	}

	/**
     * Вывести всю информацию по текущей игры
     * Успешно, возвращается массив с данными игры
     * Ошибка, возвращается массив с ключем error, кодом ошибки и описанием.
     *
     * @return array
     */

	public function show()
	{
          $game               = [];
          $game_id            = $this->game->getId();

          $game['id']         = $game_id;
          $game['state']      = $this->game->getState($game_id);
          $game['day']        = $this->game->getDay($game_id);
          $game['yards']       = $this->settings['yards_quanity'];

          $sheeps             = $this->sheep->getAlive($game_id);
          $sheepsByYard       = [];

          
          
          foreach ($sheeps as $sheep => $value) {

               $sheepsByYard[$value['yard']][] = $value['id'];

          }
          
          $game['sheeps']        = $sheepsByYard;


		return $game;
	}

	/**
     * Добавить овечку в определенный загон
     * Успешно, возвращается массив с ключем success
     * Ошибка, возвращается массив с ключем error, кодом ошибки и описанием.
     *
     * @param int 
     * @param int 
     * @return array
     */

	public function addSheep($yard)
	{
          if ( $this->currentGameState() === 'stopped' ) {
               return $this->error('4');
          }

          if ( ! is_int($yard)) {

               /* Exception type not integer */

               return $this->error();
          }

          if ( $yard < 1 || $yard > $this->settings['yards_quanity'] ) {

               /* Описать ошибку */

               return $this->error();
          }

          $game_id            = $this->game->getId();
          $day                = $this->game->getDay($game_id);
          $sheep_id           = $this->sheep->addNew($game_id, $yard); 


           /** Регистрация в журнале */

          $this->newJournalEntry($game_id, "addSheep", $day, $yard, $sheep_id);

          if ($sheep_id) {

               return $sheep_id;

          }
		
	}

	/**
     * Убрать овечка с определенным id
     * Успешно, возвращается массив с ключем success
     * Ошибка, возвращается массив с ключем error, кодом ошибки и описанием.
     *
     * @param int 
     * @return array
     */

	public function removeSheep($sheep_id)
	{    
          if ( $this->currentGameState() === 'stopped' ) {
               return $this->error('4');
          }

          if ( ! is_int($sheep_id)) {

               /* Exception type not integer */

               return $this->error();
          }

          if ( $this->sheep->kill($sheep_id) === false ) {

               /* Описать ошибку */

               return $this->error();
          }

          /** Регистрация в журнале */

          $game_id            = $this->game->getId();
          $day                = $this->game->getDay($game_id);
          $yard               = $this->sheep->getYard($sheep_id); 

          $this->newJournalEntry($game_id, "removeSheep", $day, $yard, $sheep_id);

		return $this->success;
	}

	/**
     * !!! Рефакторинг 
     * Переместить овечку в другой загон
     * Успешно, возвращается массив с ключем success
     * Ошибка, возвращается массив с ключем error, кодом ошибки и описанием.
     *
     * @param int 
     * @param int 
     * @return array
     */

	public function updateSheep($sheep_id, $yard)
	{   
          if ( $this->currentGameState() === 'stopped' ) {
               return $this->error('4');
          }

          if ( ! is_int($sheep_id)) {

               /* Exception type not integer */

               return $this->error();
          }

          if ( $yard < 1 || $yard > $this->settings['yards_quanity'] ) {

               /* Описать ошибку */

               return $this->error();
          }

          if ( $this->sheep->getState($sheep_id) !== 'alive' ) {

               return $this->error(5);
          }

	     if ( $this->sheep->setYard($sheep_id, $yard) === false ) {

               /* Описать ошибку */

               return $this->error();
          }

          /** Регистрация в журнале */

          $game_id            = $this->game->getId();
          $day                = $this->game->getDay($game_id);

          $this->newJournalEntry($game_id, "updateSheep", $day, $yard, $sheep_id);

          return $this->success;
	}

	/**
     * Вывести всю информацию по конкретной овечке
     * Успешно, возвращается массив с данными по  конкретной овечке
     * Ошибка, возвращается массив с ключем error, кодом ошибки и описанием.
     *
     * @param int 
     * @return array
     */

	public function showSheep($id)
	{
		return $this->success;
	}

	/**
     * Вывести журнал по текущей игре
     * Успешно, возвращается массив с данными по игре
     * Ошибка, возвращается массив с ключем error, кодом ошибки и описанием.
     *
     * @param int 
     * @return array
     */

	public function showJournal()
	{

          $game_id            = $this->game->getId();

          return $this->journal->getAll($game_id);
	}

     /**
     * Общий метод для возвращения ошибок
     *
     * @param int $sheeps_count
     * @return array || bool
     */

     private function error($code = null)
     {
          if ( empty($this->error[$code]) ){
               $code = "0";
          }

          $status = "error";

          $error_array = [
               'status'  => $status,
               'code'    => $code,
               'message' => $this->error[$code],
          ];

          return $error_array;
     }

     /**
     * Общий метод для успешного завершения
     *
     * @param int $sheeps_count
     * @return array || bool
     */

     private function success($code = null)
     {
          if ( empty($this->success[$code]) ){
               $code = "0";
          }

          $status = "success";

          $error_array = [
               'status'  => $status,
               'code'    => $code,
               'message' => $this->success[$code],
          ];

          return $error_array;
     }

     /**
     * Новая запись в журнал
     *
     * @param int $gameId
     * @param int $action
     * @param int $day
     * @param int $yard
     * @param int $sheep_id
     * @return  bool
     */

     private function newJournalEntry($gameId, $action, $day = 0, $yard = 0, $sheep_id = 0)
     {
          if ( ! is_int($gameId) || empty($gameId) ) {

                /* Exception type not integer */

               return false;
          }

          if ( ! isset($action) ) {
               
                /* Exception type not integer */

               return false;
          }

          return $this->journal->setEntry($gameId, $action, $day, $yard, $sheep_id);

     }

     /**
     * Сгенирировать овец для новой игры
     *
     * @param int $sheeps_count
     * @return array || bool
     */

     private function generateSheeps( $sheeps_count, $yards_quanity )
     {

          if ( ! is_int($sheeps_count) ) {

               /* Exception type not integer */

               return false;
          }

          $sheeps   = array();
          $counter  = $yards_quanity + 1;

          for ( $i = 1; $i <= $counter; $i++ ) {

               $sheeps[$i]['yard']     = $i;
               $sheeps[$i]['state']    = 'alive';
          }

          for ( $i = $counter; $i <= $sheeps_count; $i++ ) {
               $sheeps[$i]['yard']     = rand(1,$yards_quanity);
               $sheeps[$i]['state']    = 'alive';
          }

          return $sheeps;
     }

     /**
     * Добавить запись о новой игре в БД
     *
     * @param void
     * @return bool
     */

     private function newGame()
     {
           return $this->game->addNew();
     }  

     /**
     * Добавить запись в БД о завершении игры
     *
     * @param void
     * @return bool
     */

     private function endGame()
     {
          $game_id = $this->game->getId();

          return $this->game->setState($game_id,'stopped');
     }  

     /**
     * Установить иденитификатор игры для сгенерированных овечек
     *
     * @param array
     * @return bool || array
     */

     private function setGameId( $id, $sheeps )
     {
          if ( ! is_int($id) ) {
               /* Exception type not integer */
               return false;
          }

          if ( ! is_array($sheeps)) {
               /* Exception type not array */
               return fasle;
          }

          foreach ($sheeps as $key => $sheep) {
                    
                    $sheeps[$key]['game_id'] = $id;
          }

          return $sheeps;
     }

     /**
     * Сохранить сгенерированных овечек в БД
     *
     * @param array $sheeps
     * @return bool || array
     */

     private function saveGeneratedSheeps($sheeps)
     {
          if ( ! is_array($sheeps) ) {
               /* Exception type not array */
               return false;
          }
          
 
          return $this->sheep->insert($sheeps);
     }


     /**
     * Текущее состояние игры
     *
     * @param array
     * @return bool || array
     */

     private function currentGameState()
     {
          $id  = $this->game->getId();

          return $this->game->getState($id);
     }

}