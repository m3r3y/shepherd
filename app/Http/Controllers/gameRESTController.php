<?php

namespace App\Http\Controllers;

/**
 * REST Контроллер для api управления игрой
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Http\Request;
use App\Models\Services\GameBuilderInterface;
use App\Models\Services\GameBuilder;


class gameRESTController extends Controller
{
    /**
     * @property object 
     * @var GameBuilder object
     */

    protected $game;

    /**
     * Создается объект GameBuilder
     *
     * @param void
     * @return void
     */

    public function __construct()
    {
        $this->game = new GameBuilder();
    }

    /**
    *  Вывести данные по игре. Перенаправление на show
    *
    * @param void
    * @return void
    */

    public function index()
    {
        return redirect('api/game/show');
    }

    /**
     * Запуск генерации новой игры, если игра не создана.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         return $this->game->start();
    }

    /**
     * Вывести данные по игре
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        return $this->game->show();
    }


     /**
     * Добавить одинь день к текущей игре
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = null)
    {
        return $this->game->update();
    }

    /**
     * Убрать овечку с определенным id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->game->stop();
    }
}
