<?php

namespace App\Http\Controllers;

/**
 * REST Контроллер для api управления журналом
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use Illuminate\Http\Request;
use App\Models\Services\GameBuilderInterface;
use App\Models\Services\GameBuilder;

class journalRESTController extends Controller
{
    
    /**
     * @property object 
     * @var GameBuilder object
     */
    protected $game;

     /**
     * Создается объект GameBuilder
     *
     * @param void
     * @return void
     */
    public function __construct()
    {
        $this->game = new GameBuilder();
    }

    /**
     * Заглушка. Перенаправление на show
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('api/journal/show');
    }

    /**
     * Вывести журнал по текущей игре
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return $this->game->showJournal();
    }

    
}
