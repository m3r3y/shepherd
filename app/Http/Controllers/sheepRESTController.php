<?php

namespace App\Http\Controllers;

/**
 * REST Контроллер для api управления овечками
 *
 * @copyright Bimashev Merey <88mb@mail.ru>
 */

use App\Models\Services\GameBuilderInterface;
use App\Models\Services\GameBuilder;

use Illuminate\Http\Request;

class sheepRESTController extends Controller
{

    /**
     * @property object 
     * @var GameBuilder object
     */
    protected $game;

     /**
     * Создается объект GameBuilder
     *
     * @param void
     * @return void
     */
    public function __construct()
    {
        $this->game = new GameBuilder();
    }

    /**
    *  Заглушка. Перенаправление на главную.
    *
    * @param void
    * @return void
    */
    public function index()
    {
        return redirect('/');
    }


    /**
     * Добавить овечку в определенный загон
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $yard = (int) $request->yard;

        return $this->game->addSheep($yard); 
    }

    /**
    *  Заглушка. Перенаправление на главную.
    *
    * @param void
    * @return void
    */
    public function show($id)
    {
        return redirect('/'); 
    }


   /**
     * Переместить овечку в другой загон
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $yard       = (int) $request->yard;
        $sheep_id   = (int) $id;

        return $this->game->updateSheep($sheep_id, $yard); 
    }

    /**
     * Убрать овечку с определенным id
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sheep_id   = (int) $id;

        return $this->game->removeSheep($sheep_id);   
    }
}
